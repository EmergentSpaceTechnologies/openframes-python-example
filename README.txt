#
# ofpy_sharedlib_demo: A sample Python application that uses PyQt with OpenFrames
#

This sample shows how to create a CMake-based project that uses PyQt to create
a front-end GUI that contains an embedded OpenFrames 3D visualization window.

Sample features:
 - Uses CMake to setup all dependencies at configure time
   This includes OpenFrames, OpenSceneGraph, OpenVR, etc...
 - Uses a PyQt-based GUI (see python folder)
 - Sets up OpenFrames scene in a back-end shared library (see src/include folders)


How to use:
 1: Point CMake at this root folder, with an out-of-source build folder
    - See CMake documentation for instructions on how to do that

 2: When you press "Configure" in the CMake GUI, all dependencies will be downloaded
    - This will take a while the first time!!

 3: Press "Generate" in the CMake GUI to create the appropriate build system
