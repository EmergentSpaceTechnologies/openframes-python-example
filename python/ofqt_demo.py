# -*- coding: utf-8 -*-
"""
Launches a demonstration of OpenFrames managed within a PyQt5 framework

Copyright (c) 2021 Emergent Space Technologies, Inc.
"""

# OS-specific modifications before importing modules
import os
import sys
import platform
currpath = os.path.abspath(os.path.dirname(__file__))

if platform.system() == 'Windows': # Windows
    # Python 3.8 no longer searches the topmost (bin) directory
    # when loading shared library dependencies, so we must add it explicitly
    if sys.version_info[:2] >= (3,8):
        os.add_dll_directory(currpath)
else: # OSX/Linux
    # Tell OSG where to find plugins
    osglibpath = currpath + os.sep + ".." + os.sep + "lib"
    os.environ['OSG_LIBRARY_PATH'] = osglibpath
    
    if platform.system() == 'Darwin':
        # On OSX 10.15+, some fonts (e.g. Arial.ttf) are moved to the Supplemental folder
        os.environ['OSG_FILE_PATH'] = str(os.environ.get('OSG_FILE_PATH')) + os.pathsep + "/System/Library/Fonts/Supplemental"

# Import modules
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QSurfaceFormat
import OFInterfaces.PyQtOF as PyQtOF
import OFInterfaces.PyOF as PyOF

class MyOFDemoWin1(PyQtOF.OFWindow):
    """
    Inherits PyQtOF.Window for a simple window showing only a Coordinate Axes

    Attributes
    ----------
    _ref_frame_name : basestring
        The name of the primary reference frame

    """
    def __init__(self):
      """
      Instantiate a window
      """
      super().__init__(1, 1) # 1x1 window

      self._ref_frame_name = "CoordinateAxes"
      
      root = PyOF.CoordinateAxes(self._ref_frame_name)
      
      # Create a manager to handle access to the scene
      fm = PyOF.FrameManager(root);
      
      # Add the scene to the window
      self.windowProxy.setScene(fm, 0, 0);

class MyOFDemoWin2(PyQtOF.OFWindow):
    """
    Inherits PyQtOF.Window for a simple window showing only a Sphere
    
    Attributes
    ----------
    _ref_frame_name : basestring
    The name of the primary reference frame
    
    """
    def __init__(self):
      """
      Instantiate a window
      """
      super().__init__(1, 1) # 1x1 window
      
      self._ref_frame_name = "Sphere"
      
      root = PyOF.Sphere(self._ref_frame_name)
      
      # Create a manager to handle access to the scene
      fm = PyOF.FrameManager(root);
      
      # Add the scene to the window
      self.windowProxy.setScene(fm, 0, 0);

if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    # Set depth buffer and MSAA
    fmt = QSurfaceFormat()
    fmt.setDepthBufferSize(24)
    fmt.setSamples(4)
    QSurfaceFormat.setDefaultFormat(fmt)
    
    # Create first window
    ex1 = PyQtOF.OFWidget(MyOFDemoWin1)
    ex1.setWindowTitle('PyQt5 OpenFrames Window 1')
    ex1.setGeometry(50, 50, 1024, 768)
    ex1.show()
    
    # Create second window
    ex2 = PyQtOF.OFWidget(MyOFDemoWin2)
    ex2.setWindowTitle('PyQt5 OpenFrames Window 2')
    ex2.setGeometry(100, 100, 1024, 768)
    ex2.show()
    
    # Start Qt application
    ret = app.exec_()
    sys.exit(ret)
