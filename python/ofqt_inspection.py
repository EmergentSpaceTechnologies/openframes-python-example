# -*- coding: utf-8 -*-
"""
Demonstrates the use of OpenFrames from within a PyQt5 GUI application
Displays a scene with one spacecraft inspecting another spacecraft,
with the Earth in the background.

The models used here are simply for OpenFrames demo purposes, and do not
represent any real-world mission scenarios.

Copyright (c) 2021 Emergent Space Technologies, Inc.
"""

# OS-specific modifications before importing modules
import os
import sys
import platform
currpath = os.path.abspath(os.path.dirname(__file__))

if platform.system() == 'Windows': # Windows
    # Python 3.8 no longer searches the topmost (bin) directory
    # when loading shared library dependencies, so we must add it explicitly
    if sys.version_info[:2] >= (3,8):
        os.add_dll_directory(currpath)
else: # OSX/Linux
    # Tell OSG where to find plugins
    osglibpath = currpath + os.sep + ".." + os.sep + "lib"
    os.environ['OSG_LIBRARY_PATH'] = osglibpath
    
    if platform.system() == 'Darwin':
        # On OSX 10.15+, some fonts (e.g. Arial.ttf) are moved to the Supplemental folder
        os.environ['OSG_FILE_PATH'] = str(os.environ.get('OSG_FILE_PATH')) + os.pathsep + "/System/Library/Fonts/Supplemental"

# Import modules
import math
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QSurfaceFormat
import OFInterfaces.PyQtOF as PyQtOF
import OFInterfaces.PyOF as PyOF

class MyOFDemoWin1(PyQtOF.OFWindow):
    """
    Inherits PyQtOF.Window for a simple window showing only a Coordinate Axes

    Attributes
    ----------
    _frame_manager_id : int
        The FrameManager placed in the first grid location of the window proxy
    _ref_frame_name : basestring
        The name of the primary reference frame

    """
    def __init__(self):
      """
      Instantiate a window
      """
      super().__init__(1, 1) # 1x1 window
      earth_radius = 6378.0
      
      # Use Earth as root of scene
      earth = PyOF.Sphere("Earth")
      earth.setRadius(earth_radius)
      earth.setTextureMap("../data/textures/ModifiedBlueMarble.jpg")
      earth.showAxes(PyOF.ReferenceFrame.NO_AXES)
      earth.showAxesLabels(PyOF.ReferenceFrame.NO_AXES)
      earth.showNameLabel(False)
      
      distance = 40000;
      
      # Add target spacecraft
      target = PyOF.Model("Hubble")
      target.showAxes(PyOF.ReferenceFrame.NO_AXES)
      target.showAxesLabels(PyOF.ReferenceFrame.NO_AXES)
      target.showNameLabel(False)
      target.setModel("../data/models/Hubble.3ds")
      target.setPosition(earth_radius + distance, -(earth_radius + distance), 10.0)
      earth.addChild(target)
      
      # Add inspection spacecraft
      inspector = PyOF.Model("CubeSat")
      inspector.showAxes(PyOF.ReferenceFrame.NO_AXES)
      inspector.showAxesLabels(PyOF.ReferenceFrame.NO_AXES)
      inspector.showNameLabel(False)
      inspector.setModel("../data/models/Firefly-deployed.lwo")
      inspector.setModelScale(0.1, 0.1, 0.1)
      inspector.setPosition(earth_radius + distance + 100.0, -(earth_radius + distance + 100.0), -10.0)
      earth.addChild(inspector)
      
      # Rotate target to get nice orientation
      rot = PyOF.osgQuat(0.22094, 0.22094, 0.22094, 0.92388)
      target.setModelAttitude(rot)

      # Rotate inspector to point towards target
      rot = PyOF.osgQuat(0.541675, 0.541675, 0, 0.642787)
      inspector.setModelAttitude(rot)
      
      # Create a view of the inspector
      view = PyOF.View(earth, inspector, target)
      
      # Create a manager to handle access to the scene
      fm = PyOF.FrameManager(earth)
      
      # Add the scene to the window
      self.windowProxy.setScene(fm, 0, 0)
      gridPosRR = self.windowProxy.getGridPosition(0, 0)
      gridPosRR.setBackgroundColor(0, 0, 0)
      gridPosRR.setSkySphereStarData("../data/stars/inp_StarsHYGv3.txt", -2.0, 6.0, 40000)
      gridPosRR.addView(view)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    # Set depth buffer and MSAA
    fmt = QSurfaceFormat()
    fmt.setDepthBufferSize(24)
    fmt.setSamples(4)
    QSurfaceFormat.setDefaultFormat(fmt)
    
    # Create window
    ex1 = PyQtOF.OFWidget(MyOFDemoWin1)
    ex1.setWindowTitle('PyQt5 OpenFrames Window 1')
    ex1.setGeometry(50, 50, 1024, 768)
    ex1.show()
    
    # Start Qt application
    ret = app.exec_()
    sys.exit(ret)
