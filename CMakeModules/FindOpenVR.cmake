# - Find OpenVR SDK
# Find the OpenVR SDK headers and libraries.
#
# Input:
#  OpenVR_ROOT_DIR     - Location of OpenVR SDK directory
#
# Output:
#  OpenVR_INCLUDE_DIRS - where to find openvr.h, etc.
#  OpenVR_LIBRARIES    - List of libraries when using OpenVR SDK.
#  OpenVR_FOUND        - True if OpenVR SDK found.

IF (DEFINED ENV{OpenVR_ROOT_DIR})
    SET(OpenVR_ROOT_DIR "$ENV{OpenVR_ROOT_DIR}")
ENDIF()
SET(OpenVR_ROOT_DIR
    "${OpenVR_ROOT_DIR}"
    CACHE
    PATH
    "Root directory to search for OpenVR SDK")

# Look for the header file.
FIND_PATH(OpenVR_INCLUDE_DIRS NAMES openvr.h HINTS
	${OpenVR_ROOT_DIR}/headers )

# Determine architecture
IF(CMAKE_SIZEOF_VOID_P MATCHES "8")
  IF(WIN32)
    SET(_OpenVR_LIB_ARCH "win64")
  ELSEIF(APPLE)
    # OpenVR only uses 32-bit on OSX
    SET(_OpenVR_LIB_ARCH "osx32")
  ELSE()
    SET(_OpenVR_LIB_ARCH "linux64")
  ENDIF()
ELSE()
  IF(WIN32)
    SET(_OpenVR_LIB_ARCH "win32")
  ELSEIF(APPLE)
    SET(_OpenVR_LIB_ARCH "osx32")
  ELSE()
    SET(_OpenVR_LIB_ARCH "linux32")
  ENDIF()
ENDIF()

MARK_AS_ADVANCED(_OpenVR_LIB_ARCH)

# Append "d" to debug libs on windows platform
IF (WIN32)
	SET(CMAKE_DEBUG_POSTFIX d)
ENDIF()

# Locate OpenVR license file
SET(_OpenVR_LICENSE_FILE "${OpenVR_ROOT_DIR}/LICENSE")
IF(EXISTS "${_OpenVR_LICENSE_FILE}")
	SET(OpenVR_LICENSE_FILE "${_OpenVR_LICENSE_FILE}" CACHE INTERNAL "The location of the OpenVR SDK license file")
ENDIF()

# Look for the library.
FIND_LIBRARY(OpenVR_LIBRARY NAMES openvr_api HINTS ${OpenVR_ROOT_DIR}
                                                      ${OpenVR_ROOT_DIR}/lib/${_OpenVR_LIB_ARCH}
                                                    )
    
MARK_AS_ADVANCED(OpenVR_LIBRARY)
MARK_AS_ADVANCED(OpenVR_LIBRARY_DEBUG)

# No debug library for OpenVR
SET(OpenVR_LIBRARY_DEBUG ${OpenVR_LIBRARY})

SET(OpenVR_LIBRARIES optimized ${OpenVR_LIBRARY} debug ${OpenVR_LIBRARY_DEBUG})

# handle the QUIETLY and REQUIRED arguments and set OpenVR_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(OpenVR DEFAULT_MSG OpenVR_LIBRARIES OpenVR_INCLUDE_DIRS)

MARK_AS_ADVANCED(OpenVR_LIBRARIES OpenVR_INCLUDE_DIRS)
