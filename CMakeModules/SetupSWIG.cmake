# $Id$
# 
# Dependency management for projects using SWIG
# 
# Copyright (c) 2021 Emergent Space Technologies, Inc.
#
# Download and manage SWIG
#
# This approach uses FetchContent instead of ExternalProject because the latter
# does all work at build-time, which is incompatible with FIND_PACKAGE when
# later searching for OSG, OpenFrames, etc. In contrast, FetchContent downloads
# at configure-time.
# Build & install steps are also done immediately so that libraries will be
# found by FIND_PACKAGE during search.
#
# Original Author: Ravi Mathur
#
# DO NOT MODIFY THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING!

###############################################################################
# Download and setup SWIG (for OpenFrames language interfaces)
###############################################################################

SET(SWIG_SRC "${DEPENDENCY_PATH}/swig")
SET(SWIG_INSTALL_DIR "${SWIG_SRC}/installed")
SET(SWIG_VER "4.0.2")
SET(DEPENDENCY_OVERRIDE_SWIG_VERSION "" CACHE STRING "(Optional) Override the default SWIG version. Leave empty to use the default.")
IF(DEPENDENCY_OVERRIDE_SWIG_VERSION)
  SET(SWIG_VER ${DEPENDENCY_OVERRIDE_SWIG_VERSION})
ENDIF()

# SWIG_EXECUTABLE is used by CMake's FindSWIG module
IF(WIN32)
  SET(SWIG_EXECUTABLE "${SWIG_INSTALL_DIR}/swig.exe" CACHE FILEPATH "Path to SWIG executable" FORCE)
ELSE()
  SET(SWIG_EXECUTABLE "${SWIG_INSTALL_DIR}/bin/swig" CACHE FILEPATH "Path to SWIG executable" FORCE)
ENDIF()

# Don't do anything if SWIG already exists
IF(EXISTS ${SWIG_EXECUTABLE})
  MESSAGE(STATUS "SWIG ${SWIG_VER} already configured")
  
# Setup SWIG on Windows
ELSEIF(WIN32)
  # Download prebuilt SWIG on Windows
  MESSAGE(STATUS "Downloading SWIG ${SWIG_VER} ...")
  SET(SWIG_URL "http://download.sourceforge.net/swig/swigwin-${SWIG_VER}.zip")

  FetchContent_Populate(swig
    URL "${SWIG_URL}"
    TIMEOUT 60
    SOURCE_DIR ${SWIG_INSTALL_DIR}
    BINARY_DIR ${DEPENDENCY_BUILDPATH}/swig
    SUBBUILD_DIR ${DEPENDENCY_BUILDPATH}/swig-subbuild
    QUIET
  )
  
# Setup SWIG on OSX/Linux
ELSE()
  MESSAGE(STATUS "Setting up SWIG ${SWIG_VER} ...")
  SET(SWIG_URL "http://download.sourceforge.net/swig/swig-${SWIG_VER}.tar.gz")
  SET(PCRE_VER "8.43")
  SET(PCRE_FILENAME "pcre-${PCRE_VER}.tar.gz")
  SET(PCRE_URL "https://ftp.pcre.org/pub/pcre/${PCRE_FILENAME}")

  # Download SWIG
  FetchContent_Populate(swig
    URL "${SWIG_URL}"
    TIMEOUT 120
    SOURCE_DIR ${SWIG_SRC}
    BINARY_DIR ${DEPENDENCY_BUILDPATH}/swig
    SUBBUILD_DIR ${DEPENDENCY_BUILDPATH}/swig-subbuild
    QUIET
  )
  
  # Download PCRE to SWIG build folder
  IF(NOT EXISTS "${swig_BINARY_DIR}/${PCRE_FILENAME}")
    FILE(DOWNLOAD ${PCRE_URL} "${swig_BINARY_DIR}/${PCRE_FILENAME}")
  ENDIF()
  
  # Build PCRE using SWIG-provided build script
  # Note that CMake sets the CC/CXX env vars, which messes with the PCRE configure script
  # So we run that script in a modified env without CC/CXX env vars
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND} -E
      env --unset=CC --unset=CXX
      "${swig_SOURCE_DIR}/Tools/pcre-build.sh"
    WORKING_DIRECTORY ${swig_BINARY_DIR}
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/PCRE_build.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/PCRE_build.log
    #OUTPUT_QUIET ERROR_QUIET
    RESULT_VARIABLE PCRE_BUILD_RESULT
  )
  
  IF(NOT (PCRE_BUILD_RESULT EQUAL 0))
    MESSAGE(FATAL_ERROR "PCRE could not be built. See details in log file ${DEPENDENCY_LOGPATH}/PCRE_build.log")
  ENDIF()
  
  # Configure SWIG
  # This has the same CC/CXX env var issue as for PCRE above
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND} -E
      env --unset=CC --unset=CXX
      "${swig_SOURCE_DIR}/configure"
      "--prefix=${SWIG_INSTALL_DIR}"
    WORKING_DIRECTORY ${swig_BINARY_DIR}
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/SWIG_configure.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/SWIG_configure.log
    #OUTPUT_QUIET ERROR_QUIET
    RESULT_VARIABLE SWIG_CONFIGURE_RESULT
  )
  
  IF(NOT (SWIG_CONFIGURE_RESULT EQUAL 0))
    MESSAGE(FATAL_ERROR "SWIG could not be configured. See details in log file ${DEPENDENCY_LOGPATH}/SWIG_configure.log")
  ENDIF()
  
  # Build and install SWIG
  EXECUTE_PROCESS(
    COMMAND make "${UNIX_PARALLEL_OPTION}"
    WORKING_DIRECTORY ${swig_BINARY_DIR}
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/SWIG_build.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/SWIG_build.log
    #OUTPUT_QUIET ERROR_QUIET
    RESULT_VARIABLE SWIG_BUILD_RESULT
  )
  
  IF(NOT (SWIG_BUILD_RESULT EQUAL 0))
    MESSAGE(FATAL_ERROR "SWIG could not be built. See details in log file ${DEPENDENCY_LOGPATH}/SWIG_build.log")
  ENDIF()
  
  EXECUTE_PROCESS(
    COMMAND make install
    WORKING_DIRECTORY ${swig_BINARY_DIR}
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/SWIG_install.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/SWIG_install.log
    #OUTPUT_QUIET ERROR_QUIET
  )
ENDIF()

