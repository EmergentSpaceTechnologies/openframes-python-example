# $Id$
# 
# CMake-based dependency management
# 
# Copyright (c) 2021 Emergent Space Technologies, Inc.
#
# Manages third-party projects, e.g. OpenFrames, OpenSceneGraph, etc.
#
# This approach uses FetchContent instead of ExternalProject because the latter
# does all work at build-time, which is incompatible with FIND_PACKAGE when
# later searching for OSG, OpenFrames, etc. In contrast, FetchContent downloads
# at configure-time.
# Build & install steps are also done immediately so that libraries will be
# found by FIND_PACKAGE during search.
#
# Original Author: Ravi Mathur
#
# DO NOT MODIFY THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING!

INCLUDE(FetchContent)
FIND_PACKAGE(Git QUIET)

# Define macro to get the current git commit has of the specified repository
MACRO(GetGitCommit repo commitvar)  
  # Make sure repo exists
  IF(IS_DIRECTORY "${repo}")
    EXECUTE_PROCESS(
      COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
      WORKING_DIRECTORY "${repo}"
      RESULT_VARIABLE status
      OUTPUT_VARIABLE ${commitvar}
    )
        
    # Commit not returned
    IF(NOT (status EQUAL 0))
      SET(${commitvar} "0")
    ENDIF()
  ELSE()
    SET(${commitvar} "0")
  ENDIF()
ENDMACRO()

###############################################################################
# Common variables
###############################################################################

SET(DEPENDENCY_PATH "${CMAKE_CURRENT_SOURCE_DIR}/dep")
SET(DEPENDENCY_BUILDPATH "${DEPENDENCY_PATH}/build")
SET(DEPENDENCY_LOGPATH "${DEPENDENCY_PATH}/log")
FILE(MAKE_DIRECTORY "${DEPENDECY_BUILDPATH}")
FILE(MAKE_DIRECTORY "${DEPENDENCY_LOGPATH}")

# Ensure dependencies use same build type as this project
SET(COMMON_BUILD_OPTIONS
    -G${CMAKE_GENERATOR} # Use currently-selected generator
    "-DCMAKE_OSX_DEPLOYMENT_TARGET:STRING=${CMAKE_OSX_DEPLOYMENT_TARGET}" # See above for definition
)
IF(CMAKE_GENERATOR_PLATFORM) # Use currently-selected platform if defined
  SET(COMMON_BUILD_OPTIONS ${COMMON_BUILD_OPTIONS} -A${CMAKE_GENERATOR_PLATFORM})
ENDIF()
IF(CMAKE_GENERATOR_TOOLSET) # Use currently-selected toolset if defined
  SET(COMMON_BUILD_OPTIONS ${COMMON_BUILD_OPTIONS} -T${CMAKE_GENERATOR_TOOLSET})
ENDIF()

# Get number of cores for parallel build on UNIX systems
# Note that Windows will have parallel build enabled via /MP
IF(UNIX)
  INCLUDE(ProcessorCount)
  ProcessorCount(ncores)
  IF(ncores EQUAL 0)
    SET(ncores 1)
  ENDIF()
  
  SET(UNIX_PARALLEL_OPTION "-j${ncores}")
ENDIF()

# Set common paths and extensions for dependencies
if(WIN32)
  SET(DEPEND_LIB_DIR "bin")       # Directory containing shared libs
  SET(DEPEND_LIB_PATTERN "*.dll") # File pattern for shared libs
  SET(DEPEND_INSTALL_DIR "bin")     # Install directory for shared libs
elseif(APPLE)
  SET(DEPEND_LIB_DIR "lib")
  SET(DEPEND_LIB_PATTERN "*.dylib")
  SET(DEPEND_INSTALL_DIR lib)
else()
  SET(DEPEND_LIB_DIR "lib*") # Linux uses lib or lib64 dirs
  SET(DEPEND_LIB_PATTERN "*.so*") # Linux has *.so and *.so.vernum
  SET(DEPEND_INSTALL_DIR lib)
endif()

###############################################################################
# Setup dependencies
###############################################################################

INCLUDE(SetupOSG)
INCLUDE(SetupOSGEarth)
INCLUDE(SetupOpenVR)
INCLUDE(SetupSWIG)
INCLUDE(SetupOpenFrames) # Must happen after setting up OSG/OpenVR/SWIG
